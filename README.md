# FreeRADIUS and Daloradius on CentOS 8

tested on CentOS 8 (stream) on 10/9/2020

## Install CentOS 8 (stream)

Download form any one of the [mirrors](http://isoredirect.centos.org/centos/8-stream/isos/x86_64/).

- \*-boot.iso for online install
- \*-dvd1.iso for offline install

Boot with the iso and select the according setting in the installation menu.

> remember to setup root and set your user as administrator.
>
> gui is not required for this setup
>
> use auto partitioning unless you have to share disk space

## System Updates

```bash
sudo dnf -y update
```

## SELinux

```bash
sudo setenforce 0
sudo vi /etc/sysconfig/selinux
```

find the line `SELINUX=enforcing` and change it to `SELINUX=disabled`

```bash
reboot
```

## Install Apache Web Server and PHP

```bash
sudo dnf -y install @httpd @php
sudo dnf -y install php-{cli,curl,mysqlnd,devel,gd,pear,mbstring,xml,pear}
sudo pear install MDB2
```

```bash
sudo systemctl enable --now httpd php-fpm
```

```bash
sudo firewall-cmd --add-service={http,https} --permanent
sudo firewall-cmd --reload
```

## Install and Configure MariaDB Database Server

```bash
yes | sudo dnf module install mariadb
```

```bash
sudo systemctl enable --now mariadb
```

```bash
sudo mysql_secure_installation
```

> Enter current password for root (enter for none):
>
> Set root password? [Y/n] y
>
> New password: <ENTER NEW PASSWORD>
>
> Re-enter new password: <CONFIRM PASSWORD>
>
> Remove anonymous users? [Y/n] y
>
> Disallow root login remotely? [Y/n] y
>
> Remove test database and access to it? [Y/n] y
>
> Reload privilege tables now? [Y/n] y

```bash
mysql -u root -p
```

```sql
CREATE DATABASE radius;
```

```sql
GRANT ALL ON radius.* TO radius@localhost IDENTIFIED BY "StrongradIusPass";
```

```sql
FLUSH PRIVILEGES;
```

```sql
\q
```

## Installing FreeRADIUS

```bash
sudo dnf install -y @freeradius freeradius-utils freeradius-mysql
sudo systemctl enable --now radiusd.service
sudo firewall-cmd --add-service=radius --permanent
sudo firewall-cmd --reload
```

## Configure FreeRADIUS

```bash
sudo su -
```

```bash
mysql -u root -p radius < /etc/raddb/mods-config/sql/main/mysql/schema.sql
exit
```

```bash
sudo ln -s /etc/raddb/mods-available/sql /etc/raddb/mods-enabled/
sudo vi /etc/raddb/mods-available/sql
```

change the file similar to below

```bash
...
sql {
driver = "rlm_sql_mysql"
dialect = "mysql"

# Connection info:

server = "localhost"
port = 3306
login = "radius"
password = "StrongradIusPass"

# Database table configuration for everything except Oracle

radius_db = "radius"
}

# Set to ‘yes’ to read radius clients from the database (‘nas’ table)
# Clients will ONLY be read on server startup.
read_clients = yes

# Table to keep radius client info
client_table = "nas"
...
```

and this

```bash
...
        mysql {
                # If any of the files below are set, TLS encryption is enabled
                #tls {
                #       ca_file = "/etc/ssl/certs/my_ca.crt"
                #       ca_path = "/etc/ssl/certs/"
                #       certificate_file = "/etc/ssl/certs/private/client.crt"
                #       private_key_file = "/etc/ssl/certs/private/client.key"
                #       cipher = "DHE-RSA-AES256-SHA:AES128-SHA"
                #
                #       tls_required = yes
                #       tls_check_cert = no
                #       tls_check_cert_cn = no
                #}

...
```

```bash
sudo systemctl restart radiusd
```

## Install and Configure Daloradius

```bash
sudo dnf -y install wget
wget https://github.com/lirantal/daloradius/archive/master.zip
unzip master.zip
mv daloradius-master/ daloradius
cd daloradius
```

```bash
mysql -u root -p radius < contrib/db/fr2-mysql-daloradius-and-freeradius.sql
```

```bash
mysql -u root -p radius < contrib/db/mysql-daloradius.sql
```

```bash
cd ..
sudo mv daloradius /var/www/html/
sudo chown -R apache:apache /var/www/html/daloradius/
sudo chmod 664 /var/www/html/daloradius/library/daloradius.conf.php
sudo vi /var/www/html/daloradius/library/daloradius.conf.php
```

edit the following values as bellow

```bash
$configValues['CONFIG_DB_HOST'] = 'localhost';
$configValues['CONFIG_DB_PORT'] = '3306';
$configValues['CONFIG_DB_USER'] = 'radius';
$configValues['CONFIG_DB_PASS'] = 'StrongradIusPass';
$configValues['CONFIG_DB_NAME'] = 'radius';
```

```bash
sudo systemctl restart radiusd.service httpd
sudo semanage fcontext -a -t httpd_sys_rw_content_t "/var/www/html/daloradius(/.*)?"
sudo restorecon -Rv /var/www/html/daloradius
```

## Access daloRADIUS Web Interface

http://server_ip_or_hostname/daloradius.

login details

> Username: administrator
>
> Password: radius

## Radius test

```bash
su
```

```bash
echo $'client my_lan {\n    ipaddr = 172.16.4.0/24\n    secret = testing123\n    require_message_authenticator = no\n}' >> /etc/raddb/clients.conf && exit
```

> change ipaddr = 172.16.4.0/24 to the same one as your test machine is

## Access logging

```bash
sudo chmod -R 777 /var/log/messages /var/log/radius/radius.log
sudo ln /var/log/radius/radius.log /var/log/freeradius/radius.log
```

## Files for customization

- /var/www/html/daloradius/index.php
- /var/www/html/daloradius/page-footer.php
- /var/www/html/daloradius/include/menu/menu-items.php
- /var/www/html/daloradius/library/exten-welcome_page.php
